package me.smourad.housing.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HousingHouse {

    private Long id;
    private String name;
    private String worldName;
    private List<HousingPoint> zone = new ArrayList<>();
    private Integer maxCoOwners;
    private Double pricePerMonth;

}
