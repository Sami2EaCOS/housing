package me.smourad.housing.data;

import lombok.Data;

import java.util.UUID;

@Data
public class HousingConfig {

    private UUID adminUUID;

}
