package me.smourad.housing.data;

import lombok.Data;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
public class HousingAgreement {

    private UUID owner;
    private Set<UUID> coOwners;
    private Long houseId;
    private Instant lastPayment;

}
