package me.smourad.housing.data;

import lombok.Data;

@Data
public class HousingPoint {

    private Integer x;
    private Integer y;
    private Integer z;

}
