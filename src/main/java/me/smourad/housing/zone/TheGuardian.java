package me.smourad.housing.zone;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

@Singleton
public class TheGuardian implements Listener {

    private final TheLandLord landLord;

    @Inject
    public TheGuardian(JavaPlugin plugin, TheLandLord landLord) {
        this.landLord = landLord;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (!GameMode.CREATIVE.equals(player.getGameMode())
                && Objects.nonNull(landLord.getHouseOnBlock(block))) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteraction(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (Objects.isNull(block)) {
            return;
        }

        if (!GameMode.CREATIVE.equals(player.getGameMode())
                && Objects.nonNull(landLord.getHouseOnBlock(block))
                && (!landLord.canInteractWith(player, block))
        ) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Long houseId = landLord.getHouseOnPlayer(player);

        if (!GameMode.CREATIVE.equals(player.getGameMode())
                && Objects.nonNull(houseId)
                && (!landLord.canEnter(player, houseId))
                && landLord.isRented(houseId)
        ) {
            teleportPlayerOut(player);
        }
    }

    protected void teleportPlayerOut(Player player) {
        int radius = TheLandLord.TELEPORT_EXPULSION_RANGE;

        Block block;
        do {
            block = landLord.getNearestBlockOutOfHomes(player, radius++);
        } while (Objects.isNull(block));

        player.teleport(block.getLocation());
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (!GameMode.CREATIVE.equals(player.getGameMode())
                && Objects.nonNull(landLord.getHouseOnBlock(block))) {
            event.setCancelled(true);
        }
    }

}
