package me.smourad.housing.zone;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.data.HousingAgreement;
import me.smourad.housing.data.HousingHouse;
import me.smourad.housing.data.HousingPoint;
import me.smourad.housing.exception.HousingException;
import me.smourad.housing.utils.TheLibrarian;
import me.smourad.housing.vault.VaultConnector;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.awt.*;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class TheLandLord {

    protected static final String HOUSE_DATA_FOLDER = "house";
    protected static final String AGREEMENT_DATA_FOLDER = "agreement";
    public static final Integer TELEPORT_EXPULSION_RANGE = 5;

    private final Path dataFolder;
    private final Gson gson;
    private final JavaPlugin plugin;
    private final TheLibrarian librarian;
    private final VaultConnector vaultConnector;

    private final Map<Long, HousingHouse> houses = new HashMap<>();
    private final Map<UUID, Set<Long>> authorizedHomesForPlayer = new HashMap<>();
    private final Map<Long, HousingAgreement> agreements = new HashMap<>();
    private final Map<Block, Long> houseBlocks = new HashMap<>();
    private Long counter;

    @Inject
    public TheLandLord(JavaPlugin plugin, Gson gson, TheLibrarian librarian, VaultConnector vaultConnector) {
        this.gson = gson;
        this.librarian = librarian;
        this.vaultConnector = vaultConnector;
        this.plugin = plugin;
        this.dataFolder = plugin.getDataFolder().toPath();

        load();
    }

    public boolean canInteractWith(Player player, Block block) {
        if (houseBlocks.containsKey(block)) {
            Long id = houseBlocks.get(block);
            return authorizedHomesForPlayer
                    .getOrDefault(player.getUniqueId(), new HashSet<>())
                    .contains(id);
        }

        return true;
    }

    public HousingHouse getHouse(Long houseId) {
        return houses.get(houseId);
    }

    public HousingAgreement getAgreementOfHouse(Long houseId) {
        return agreements.get(houseId);
    }

    public @Nullable Long getHouseOnPlayer(Player player) {
        Location location = player.getLocation();
        Block block = player.getWorld().getBlockAt(location);

        return getHouseOnBlock(block);
    }

    public @Nullable Long getHouseOnBlock(Block block) {
        return houseBlocks.get(block);
    }

    public void deleteHouse(Long houseId) {
        houseBlocks.entrySet().removeIf(entry -> entry.getValue().equals(houseId));
        houses.remove(houseId);
        agreements.remove(houseId);
        authorizedHomesForPlayer.forEach(((uuid, houseIds) -> houseIds.remove(houseId)));

        String filename = String.valueOf(houseId);
        librarian.delete(dataFolder.resolve(AGREEMENT_DATA_FOLDER), filename);
        librarian.delete(dataFolder.resolve(HOUSE_DATA_FOLDER), filename);
    }

    public void deleteAgreement(Long houseId) {
        agreements.remove(houseId);
        authorizedHomesForPlayer.forEach(((uuid, houseIds) -> houseIds.remove(houseId)));
        librarian.delete(dataFolder.resolve(AGREEMENT_DATA_FOLDER), String.valueOf(houseId));
    }

    public Block getNearestBlockOutOfHomes(Player player, int radius) {
        World world = player.getWorld();
        Location location = player.getLocation();

        int baseX = location.getBlockX();
        int baseY = location.getBlockY();
        int baseZ = location.getBlockZ();

        for (int x = baseX - radius; x <= baseX + radius; x++) {
            for (int y = baseY - radius; y <= baseY + radius; y++) {
                for (int z = baseZ - radius; z <= baseZ + radius; z++) {
                    Block block = world.getBlockAt(x, y, z);
                    if (!houseBlocks.containsKey(block) && isBlockSafe(block)) {
                        return block;
                    }
                }
            }
        }

        return null;
    }

    protected boolean isBlockSafe(Block block) {
        World world = block.getWorld();
        Block head = world.getBlockAt(block.getX(), block.getY() + 1, block.getZ());
        Block ground = world.getBlockAt(block.getX(), block.getY() - 1, block.getZ());

        return head.getType().isAir() && block.getType().isAir() && ground.getType().isBlock();
    }

    public boolean canEnter(Player player, Long houseId) {
        return authorizedHomesForPlayer.getOrDefault(player.getUniqueId(), new HashSet<>()).contains(houseId);
    }

    public boolean isRented(Long houseId) {
        return agreements.containsKey(houseId);
    }

    public boolean rent(Player player, Long houseId) {
        return rent(player, houses.get(houseId));
    }

    public boolean rent(Player player, HousingHouse house) {
        if (agreements.containsKey(house.getId())) {
            return false;
        }

        Economy economy = vaultConnector.getEconomy();
        Double pricePerMonth = house.getPricePerMonth();
        if (economy.getBalance(player) >= pricePerMonth) {
            createAgreement(player, house);
            economy.withdrawPlayer(player, pricePerMonth);
            return true;
        }

        return false;
    }

    public boolean addCoOwner(OfflinePlayer player, Long houseId) {
        HousingAgreement agreement = agreements.get(houseId);
        HousingHouse house = houses.get(houseId);

        if (house.getMaxCoOwners() > agreement.getCoOwners().size()) {
            agreement.getCoOwners().add(player.getUniqueId());
            saveAgreement(agreement);
            return true;
        }

        return false;
    }

    public void removeCoOwner(OfflinePlayer player, Long houseId) {
        HousingAgreement agreement = agreements.get(houseId);

        agreement.getCoOwners().remove(player.getUniqueId());
        saveAgreement(agreement);
    }

    protected void createAgreement(Player player, HousingHouse house) {
        HousingAgreement agreement = new HousingAgreement()
                .setHouseId(house.getId())
                .setOwner(player.getUniqueId())
                .setCoOwners(new HashSet<>())
                .setLastPayment(Instant.now());

        saveAgreement(agreement);
    }

    protected void saveAgreement(HousingAgreement agreement) {
        Long houseId = agreement.getHouseId();
        cleanAuthorization(houseId);

        agreements.put(houseId, agreement);
        authorize(agreement.getOwner(), houseId);
        agreement.getCoOwners().forEach(uuid -> authorize(uuid, houseId));

        new BukkitRunnable() {
            @Override
            public void run() {
                librarian.write(dataFolder.resolve(AGREEMENT_DATA_FOLDER), String.valueOf(agreement.getHouseId()), gson.toJson(agreement));
            }
        }.runTaskAsynchronously(plugin);
    }

    protected void cleanAuthorization(Long houseId) {
        authorizedHomesForPlayer.values().forEach(authorization -> authorization.remove(houseId));
    }

    protected void authorize(UUID playerUUID, Long houseId) {
        authorizedHomesForPlayer.putIfAbsent(playerUUID, new HashSet<>());
        authorizedHomesForPlayer.get(playerUUID).add(houseId);
    }

    public void createHome(String name, Double pricePerMonth, Integer maxCoOwner, List<Block> blocks) {
        World world = blocks.get(0).getWorld();

        HousingHouse house = new HousingHouse()
                .setId(counter++)
                .setZone(createZone(blocks))
                .setWorldName(world.getName())
                .setName(name)
                .setMaxCoOwners(maxCoOwner)
                .setPricePerMonth(pricePerMonth);

        saveHouse(house);
    }

    protected List<HousingPoint> createZone(List<Block> blocks) {
        return blocks.stream()
                .map(this::createPoint)
                .collect(Collectors.toList());
    }

    protected HousingPoint createPoint(Block location) {
        return new HousingPoint()
                .setX(location.getX())
                .setY(location.getY())
                .setZ(location.getZ());
    }

    public void saveHouse(HousingHouse house) {
        Long id = house.getId();
        houses.put(id, house);
        houseBlocks.entrySet().removeIf(entry -> entry.getValue().equals(id));

        List<Block> blocksOfHouse = getBlocksOfHouse(house);
        blocksOfHouse.forEach(block -> {
            if (houseBlocks.containsKey(block)) {
                throw new HousingException();
            }

            houseBlocks.put(block, id);
        });


        new BukkitRunnable() {
            @Override
            public void run() {
                librarian.write(dataFolder.resolve(HOUSE_DATA_FOLDER), String.valueOf(id), gson.toJson(house));
            }
        }.runTaskAsynchronously(plugin);
    }

    protected List<Block> getBlocksOfHouse(HousingHouse house) {
        Polygon polygon = new Polygon();
        List<HousingPoint> zone = house.getZone();
        for (HousingPoint point : zone) {
            polygon.addPoint(point.getX(), point.getZ());
        }

        World world = Bukkit.getWorld(house.getWorldName());
        if (Objects.isNull(world)) {
            throw new HousingException();
        }

        int minY = zone.stream().mapToInt(HousingPoint::getY).min().orElseThrow(HousingException::new);
        int maxY = zone.stream().mapToInt(HousingPoint::getY).max().orElseThrow(HousingException::new);
        List<Block> blocks = new ArrayList<>();
        for (int x = polygon.getBounds().x; x <= polygon.getBounds().x + polygon.getBounds().width; x++) {
            for (int z = polygon.getBounds().y; z <= polygon.getBounds().y + polygon.getBounds().height; z++) {
                if (polygon.contains(x, z)) {
                    for (int y = minY; y <= maxY; y++) {
                        blocks.add(world.getBlockAt(x, y, z));
                    }
                }
            }
        }

        return blocks;
    }

    protected void load() {
        librarian.readAll(dataFolder.resolve(HOUSE_DATA_FOLDER)).stream()
                .map(json -> gson.fromJson(json, HousingHouse.class))
                .forEach(house -> {
                    Long houseId = house.getId();
                    houses.put(houseId, house);
                    getBlocksOfHouse(house).forEach(block -> houseBlocks.put(block, houseId));
                });

        librarian.readAll(dataFolder.resolve(AGREEMENT_DATA_FOLDER)).stream()
                .map(json -> gson.fromJson(json, HousingAgreement.class))
                .forEach(agreement -> {
                    Long houseId = agreement.getHouseId();
                    agreements.put(houseId, agreement);
                    authorize(agreement.getOwner(), houseId);
                    agreement.getCoOwners().forEach(uuid -> authorize(uuid, houseId));
                });

        counter = houses.keySet().stream()
                .max(Long::compare)
                .orElse(1L) + 1;
    }

    public void changeOwner(HousingHouse house, UUID playerUUID) {
        Long houseId = house.getId();

        if (Objects.isNull(playerUUID)) {
            deleteAgreement(houseId);
            return;
        }

        HousingAgreement agreement = getAgreementOfHouse(houseId);
        agreement.setOwner(playerUUID);
        agreement.getCoOwners().removeIf(playerUUID::equals);

        saveAgreement(agreement);
    }

    public Collection<HousingAgreement> getAgreements() {
        return agreements.values();
    }
}
