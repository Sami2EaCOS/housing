package me.smourad.housing.zone;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.data.HousingAgreement;
import me.smourad.housing.data.HousingHouse;
import me.smourad.housing.utils.TheJournalist;
import me.smourad.housing.vault.VaultConnector;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class TheCollector {

    protected static final Long PAYMENT_LOOP_DELAY = 60L;

    private final JavaPlugin plugin;
    private final TheLandLord landLord;
    private final VaultConnector vaultConnector;
    private final TheJournalist journalist;

    @Inject
    public TheCollector(
            JavaPlugin plugin,
            TheLandLord landLord,
            VaultConnector vaultConnector,
            TheJournalist journalist
    ) {
        this.plugin = plugin;
        this.landLord = landLord;
        this.vaultConnector = vaultConnector;
        this.journalist = journalist;

        waitAndCollect();
    }

    public void waitAndCollect() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Collection<HousingAgreement> agreements = landLord.getAgreements();
                List<HousingAgreement> toPay = agreements.stream()
                        .filter(agreement -> agreement.getLastPayment()
                                .plus(7, ChronoUnit.DAYS)
                                .isBefore(Instant.now())
                        )
                        .collect(Collectors.toList());

                toPay.forEach(agreement -> {
                    Long houseId = agreement.getHouseId();
                    HousingHouse house = landLord.getHouse(houseId);

                    Economy economy = vaultConnector.getEconomy();
                    Double pricePerMonth = house.getPricePerMonth();

                    OfflinePlayer player = Bukkit.getOfflinePlayer(agreement.getOwner());

                    if (economy.getBalance(player) >= pricePerMonth) {
                        economy.withdrawPlayer(player, pricePerMonth);
                    } else {
                        agreement.setOwner(journalist.getConfig().getAdminUUID());
                        landLord.saveAgreement(agreement);
                    }
                });
            }
        }.runTaskTimer(plugin, PAYMENT_LOOP_DELAY * 20L, PAYMENT_LOOP_DELAY * 20L);
    }


}
