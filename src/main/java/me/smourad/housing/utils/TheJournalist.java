package me.smourad.housing.utils;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.data.HousingConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Path;

@Singleton
public class TheJournalist {

    protected static final String CONFIG_FILE_NAME = "config";
    protected static final String CONFIG_FILE = CONFIG_FILE_NAME + ".json";

    private final Path dataFolder;
    private final TheLibrarian librarian;
    private final Gson gson;

    private final HousingConfig config;

    @Inject
    public TheJournalist(JavaPlugin plugin, TheLibrarian librarian, Gson gson) throws IOException {
        this.dataFolder = plugin.getDataFolder().toPath();
        this.librarian = librarian;
        this.gson = gson;
        this.config = loadConfig();
    }

    public HousingConfig getConfig() {
        return config;
    }

    public HousingConfig loadConfig() throws IOException {
        if (librarian.exist(dataFolder, CONFIG_FILE_NAME)) {
            String data = librarian.read(dataFolder, CONFIG_FILE_NAME);
            return gson.fromJson(data, HousingConfig.class);
        } else {
            try (InputStream input = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE)) {
                assert input != null;

                InputStreamReader reader = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(reader);

                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line);
                }
                String data = builder.toString();
                librarian.write(dataFolder, CONFIG_FILE, data);

                return gson.fromJson(data, HousingConfig.class);
            }
        }

    }

}
