package me.smourad.housing.utils;

import java.time.ZoneId;

public class HousingConstant {

    public static final String SPACE = " ";
    public static final ZoneId FRANCE_ZONE_ID = ZoneId.of("Europe/Paris");

}
