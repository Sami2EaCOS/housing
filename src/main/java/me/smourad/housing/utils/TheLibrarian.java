package me.smourad.housing.utils;

import com.google.inject.Singleton;
import me.smourad.housing.exception.HousingException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class TheLibrarian {

    public String read(Path dir, String filename) {
        try {
            Files.createDirectories(dir);
            Path path = dir.resolve(String.format("%s.json", filename));
            return read(path);
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

    public void write(Path dir, String filename, String json) {
        try {
            Files.createDirectories(dir);
            Path path = dir.resolve(String.format("%s.json", filename));
            write(path, json.getBytes());
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

    public void delete(Path dir, String filename) {
        Path path = dir.resolve(String.format("%s.json", filename));
        delete(path);
    }

    protected void delete(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

    public List<String> readAll(Path dir) {
        try {
            Files.createDirectories(dir);
            try (Stream<Path> files = Files.walk(dir)) {
                return files
                        .filter(Files::isRegularFile)
                        .map(this::read)
                        .collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

    protected String read(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

    public boolean exist(Path dir, String filename) {
        Path path = dir.resolve(String.format("%s.json", filename));
        return exist(path);
    }

    protected boolean exist(Path path) {
        return Files.exists(path);
    }

    protected void write(Path path, byte[] data) {
        try {
            Files.write(path, data);
        } catch (IOException e) {
            throw new HousingException(e);
        }
    }

}
