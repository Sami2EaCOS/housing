package me.smourad.housing.exception;

public class HousingException extends RuntimeException {

    public HousingException() {
        super();
    }

    public HousingException(String message) {
        super(message);
    }

    public HousingException(String message, Throwable cause) {
        super(message, cause);
    }

    public HousingException(Throwable cause) {
        super(cause);
    }

}
