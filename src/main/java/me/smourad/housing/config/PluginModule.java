package me.smourad.housing.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import me.smourad.housing.config.adapter.InstantAdapter;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;

import java.time.Instant;
import java.util.Set;

public class PluginModule extends AbstractModule {

    private final JavaPlugin plugin;

    public PluginModule(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void configure() {
        bind(JavaPlugin.class).toInstance(this.plugin);

        loadGson();
        loadSingleton();
    }

    private void loadSingleton() {
        Reflections reflections = new Reflections("me.smourad.housing");
        Set<Class<?>> singleton = reflections.getTypesAnnotatedWith(Singleton.class);
        singleton.forEach(klass -> bind(klass).asEagerSingleton());
    }

    private void loadGson() {
        bind(Gson.class).toInstance(new GsonBuilder()
                .registerTypeAdapter(Instant.class, new InstantAdapter())
                .create()
        );
    }

    public Injector createInjector() {
        return Guice.createInjector(this);
    }

}
