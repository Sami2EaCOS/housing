package me.smourad.housing.config.adapter;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class InstantAdapter implements JsonDeserializer<Instant>, JsonSerializer<Instant> {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter
            .ofPattern(DATE_FORMAT)
            .withZone(ZoneOffset.UTC);

    @Override
    public Instant deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        return DATE_FORMATTER.parse(json.getAsString(), Instant::from);
    }

    @Override
    public JsonElement serialize(Instant instant, Type type, JsonSerializationContext context) {
        return context.serialize(DATE_FORMATTER.format(instant));
    }

}
