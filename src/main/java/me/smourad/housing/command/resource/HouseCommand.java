package me.smourad.housing.command.resource;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.command.HousingCommand;
import me.smourad.housing.data.HousingAgreement;
import me.smourad.housing.data.HousingHouse;
import me.smourad.housing.utils.HousingConstant;
import me.smourad.housing.zone.TheLandLord;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class HouseCommand extends HousingCommand {

    protected static final String HOUSE_INFO = "info";
    protected static final String HOUSE_ADD = "add";
    protected static final String HOUSE_REMOVE = "remove";
    protected static final String HOUSE_ADMIN_SET = "set";
    protected static final String HOUSE_ADMIN_DELETE = "delete";
    protected static final String HOUSE_ADMIN_FREE = "free";

    protected static final String HOUSE_OWNER_FIELD = "owner";
    protected static final String HOUSE_MAX_CO_OWNERS_FIELD = "maxCoOwners";
    protected static final String HOUSE_PRICE_PER_MONTH_FIELD = "pricePerMonth";

    private final TheLandLord landLord;
    private final Gson gson;

    @Inject
    public HouseCommand(TheLandLord landLord, Gson gson) {
        super("house");

        this.landLord = landLord;
        this.gson = gson;
    }

    @Override
    public boolean onCommand(Player player, String[] args) {
        Long houseId = landLord.getHouseOnPlayer(player);

        if (Objects.isNull(houseId)) {
            player.sendMessage(ChatColor.YELLOW + "Vous n'êtes pas dans une maison");
            return true;
        } else {
            switch (args.length) {
                case 1:
                    if (args[0].equalsIgnoreCase(HOUSE_INFO)) {
                        printInfo(player, houseId);
                        return true;
                    } else if (args[0].equalsIgnoreCase(HOUSE_ADMIN_DELETE)) {
                        landLord.deleteHouse(houseId);
                        return true;
                    } else if (args[0].equalsIgnoreCase(HOUSE_ADMIN_FREE)) {
                        landLord.deleteAgreement(houseId);
                        return true;
                    } else {
                        return false;
                    }
                case 2:
                    return manageCoOwners(houseId, player, args);
                case 3:
                    return update(houseId, player, args);
                default:
                    return false;
            }
        }
    }

    protected boolean update(Long houseId, Player player, String... args) {
        if (!args[0].equalsIgnoreCase(HOUSE_ADMIN_SET)) {
            return false;
        }

        HousingHouse house = landLord.getHouse(houseId);

        switch (args[1]) {
            case HOUSE_MAX_CO_OWNERS_FIELD:
                house.setMaxCoOwners(Integer.valueOf(args[2]));
                break;
            case HOUSE_PRICE_PER_MONTH_FIELD:
                house.setPricePerMonth(Double.valueOf(args[2]));
                break;
            case HOUSE_OWNER_FIELD:
                Player owner = Bukkit.getPlayer(args[2]);
                if (Objects.isNull(owner)) {
                    player.sendMessage(ChatColor.YELLOW + "Le joueur n'est pas connecté");
                    return true;
                }

                landLord.changeOwner(house, owner.getUniqueId());
                break;
            default:
                break;
        }

        landLord.saveHouse(house);

        return true;
    }

    protected boolean manageCoOwners(Long houseId, Player player, String... args) {
        HousingAgreement agreement = landLord.getAgreementOfHouse(houseId);

        if (Objects.isNull(agreement) || !agreement.getOwner().equals(player.getUniqueId())) {
            player.sendMessage(ChatColor.YELLOW + "Vous ne louez pas cette maison");
            return true;
        }

        if (args[0].equalsIgnoreCase(HOUSE_ADD)) {
            Player coOwner = Bukkit.getPlayer(args[1]);
            if (Objects.isNull(coOwner)) {
                player.sendMessage(ChatColor.YELLOW + args[1] + " n'est pas connecté");
                return true;
            }

            UUID coOwnerUUID = coOwner.getUniqueId();

            if (agreement.getCoOwners().contains(coOwnerUUID) || agreement.getOwner().equals(coOwnerUUID)) {
                player.sendMessage(ChatColor.YELLOW + args[1] + " est déjà ajouté dans cette maison");
            } else if (landLord.addCoOwner(coOwner, houseId)) {
                player.sendMessage(ChatColor.YELLOW + args[1] + " ajouté à la maison en tant que co-propriétaire");
            } else {
                player.sendMessage(ChatColor.YELLOW + "Vous ne pouvez plus rajouter de co-propriétaire");
            }
            return true;
        }

        if (args[0].equalsIgnoreCase(HOUSE_REMOVE)) {
            Optional<OfflinePlayer> selectedCoOwner = agreement.getCoOwners().stream()
                    .map(Bukkit::getOfflinePlayer)
                    .filter(p -> args[1].equalsIgnoreCase(p.getName()))
                    .findFirst();

            if (!selectedCoOwner.isPresent()) {
                player.sendMessage(ChatColor.YELLOW + "Le joueur n'est pas co-propriétaire");
            } else {
                landLord.removeCoOwner(selectedCoOwner.get(), houseId);
                player.sendMessage(ChatColor.YELLOW + args[1] + " retiré de la maison en tant que co-propriétaire");
            }
            return true;
        }

        return false;
    }

    protected void printInfo(Player player, Long houseId) {
        HousingHouse house = landLord.getHouse(houseId);
        HousingAgreement agreement = landLord.getAgreementOfHouse(houseId);

        List<String> infos = new ArrayList<>();
        infos.add(String.format("%sMaison %d du nom de %s", ChatColor.YELLOW, houseId, house.getName()));
        if (Objects.nonNull(agreement)) {
            infos.add(String.format("%sLocataire: %s", ChatColor.YELLOW, Bukkit.getOfflinePlayer(agreement.getOwner()).getName()));
            infos.add(String.format("%sCo-Locataire(s) (%d/%d): %s",
                    ChatColor.YELLOW,
                    agreement.getCoOwners().size(),
                    house.getMaxCoOwners(),
                    StringUtils.join(agreement.getCoOwners().stream()
                                    .map(Bukkit::getOfflinePlayer)
                                    .map(OfflinePlayer::getName)
                                    .collect(Collectors.toList()),
                            HousingConstant.SPACE
                    )));
            infos.add(String.format("%sDate du dernier paiement: %s",
                    ChatColor.YELLOW,
                    gson.toJson(agreement.getLastPayment())
            ));
            infos.add(String.format("%sDate du prochain paiement: %s",
                    ChatColor.YELLOW,
                    gson.toJson(agreement.getLastPayment().plus(7, ChronoUnit.DAYS))
            ));
        } else {
            infos.add(ChatColor.YELLOW + "La maison est libre");
        }
        infos.add(String.format("%sPrix par semaine: %f",
                ChatColor.YELLOW,
                house.getPricePerMonth()
        ));

        infos.forEach(player::sendMessage);
    }

    @Override
    public @Nullable List<String> onTabComplete(Player player, String[] args) {
        switch (args.length) {
            case 1:
                return Arrays.asList(HOUSE_INFO, HOUSE_ADD, HOUSE_REMOVE, HOUSE_ADMIN_SET, HOUSE_ADMIN_DELETE, HOUSE_ADMIN_FREE);
            case 2:
                if (args[0].equalsIgnoreCase(HOUSE_ADMIN_SET)) {
                    return Arrays.asList(HOUSE_OWNER_FIELD, HOUSE_PRICE_PER_MONTH_FIELD, HOUSE_MAX_CO_OWNERS_FIELD);
                } else if (args[0].equalsIgnoreCase(HOUSE_ADD)) {
                    return Bukkit.getOnlinePlayers().stream()
                            .map(Player::getName)
                            .collect(Collectors.toList());
                } else if (args[0].equalsIgnoreCase(HOUSE_REMOVE)) {
                    Long houseId = landLord.getHouseOnPlayer(player);
                    HousingAgreement agreement = landLord.getAgreementOfHouse(houseId);
                    return Objects.nonNull(houseId) && Objects.nonNull(agreement)
                            ? agreement.getCoOwners().stream()
                            .map(Bukkit::getOfflinePlayer)
                            .map(OfflinePlayer::getName)
                            .collect(Collectors.toList())
                            : null;
                } else {
                    return Collections.emptyList();
                }
            case 3:
                if (args[0].equalsIgnoreCase(HOUSE_ADMIN_SET) && args[1].equalsIgnoreCase(HOUSE_OWNER_FIELD)) {
                    return Bukkit.getOnlinePlayers().stream()
                            .map(Player::getName)
                            .collect(Collectors.toList());
                } else {
                    return Collections.emptyList();
                }
            default:
                return Collections.emptyList();
        }
    }
}
