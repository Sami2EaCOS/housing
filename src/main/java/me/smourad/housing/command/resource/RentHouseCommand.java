package me.smourad.housing.command.resource;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.command.HousingCommand;
import me.smourad.housing.zone.TheLandLord;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

@Singleton
public class RentHouseCommand extends HousingCommand {

    private final TheLandLord landLord;

    @Inject
    public RentHouseCommand(TheLandLord landLord) {
        super("rent");

        this.landLord = landLord;
    }

    @Override
    public boolean onCommand(Player player, String[] args) {
        Long houseId = landLord.getHouseOnPlayer(player);

        if (Objects.isNull(houseId)) {
            player.sendMessage(ChatColor.YELLOW + "Vous n'êtes pas dans une maison disponible");
        } else {
            if (landLord.rent(player, houseId)) {
                player.sendMessage(ChatColor.YELLOW + "Vous êtes maintenant locataires des lieux");
            } else {
                player.sendMessage(ChatColor.YELLOW + "Vous n'avez pas les fonds");
            }
        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(Player player, String[] args) {
        return null;
    }
}
