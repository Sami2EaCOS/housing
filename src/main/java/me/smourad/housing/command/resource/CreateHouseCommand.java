package me.smourad.housing.command.resource;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.smourad.housing.command.HousingCommand;
import me.smourad.housing.zone.TheLandLord;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.RayTraceResult;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@Singleton
public class CreateHouseCommand extends HousingCommand implements Listener {

    protected static final double CREATIVE_RANGE = 5;
    protected static final double NORMAL_RANGE = 4.5;
    protected static final String ON_THE_SPOT_STRING = "~";

    private final TheLandLord landLord;
    private final Map<UUID, List<Block>> selectedBlocks = new HashMap<>();

    @Inject
    public CreateHouseCommand(JavaPlugin plugin, TheLandLord landLord) {
        super("create-house");

        this.landLord = landLord;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(Player player, String[] args) {
        if (args.length == 3) {
            List<Block> selection = selectedBlocks.get(player.getUniqueId());
            if (selection.isEmpty()) {
                player.sendMessage(ChatColor.YELLOW + "Aucune selection en cours");
                return true;
            }

            landLord.createHome(args[0], Double.valueOf(args[1]), Integer.valueOf(args[2]), selection);
            player.sendMessage(ChatColor.YELLOW + "Zone créée");

            return true;
        }

        return false;
    }

    @EventHandler
    public void onHotbarChange(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        selectedBlocks.getOrDefault(player.getUniqueId(), new ArrayList<>()).clear();
    }

    @EventHandler
    public void onInteractWithPrismarineShard(PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        if (Objects.isNull(item) || !item.getType().equals(Material.PRISMARINE_SHARD)) {
            return;
        }

        Player player = event.getPlayer();
        World world = player.getWorld();
        Location location = player.getEyeLocation();

        double range = GameMode.CREATIVE.equals(player.getGameMode())
                ? CREATIVE_RANGE
                : NORMAL_RANGE;

        RayTraceResult rayTraceResult = world.rayTraceBlocks(location, location.getDirection(), range);
        if (Objects.nonNull(rayTraceResult) && Objects.nonNull(rayTraceResult.getHitBlock())) {
            UUID playerUUID = player.getUniqueId();
            selectedBlocks.putIfAbsent(playerUUID, new ArrayList<>());
            selectedBlocks.get(playerUUID).add(rayTraceResult.getHitBlock());

            player.sendMessage(ChatColor.YELLOW + "Bloc selectionné");
        }
    }


    @Override
    public @Nullable List<String> onTabComplete(Player player, String[] args) {
        return Collections.emptyList();
    }

    protected List<String> getLocationAutocomplete(Player player, String... info) {
        World world = player.getWorld();
        Location location = player.getEyeLocation();

        double range = GameMode.CREATIVE.equals(player.getGameMode())
                ? CREATIVE_RANGE
                : NORMAL_RANGE;

        RayTraceResult rayTraceResult = world.rayTraceBlocks(location, location.getDirection(), range);
        String str;
        if (Objects.isNull(rayTraceResult) || Objects.isNull(rayTraceResult.getHitBlock())) {
            StringBuilder builder = new StringBuilder();
            for (int i=0; i<3-info.length; i++) {
                builder.append(ON_THE_SPOT_STRING);

                if (i < 3-info.length) {
                    builder.append(" ");
                }
            }
            str = builder.toString();
        } else {
            Block target = rayTraceResult.getHitBlock();
            str = getBlockLocationString(target, info);
        }

        return convertStringToMultiplesInput(str);
    }

    protected String getBlockLocationString(Block block, String... info) {
        StringBuilder builder = new StringBuilder();
        for (int i=0; i<info.length; i++) {
            builder.append(ON_THE_SPOT_STRING).append(" ");
        }

        Location targetLocation = block.getLocation();
        for (int i=info.length; i<3; i++) {
            switch (i) {
                case 0:
                    builder.append(targetLocation.getBlockX()).append(" ");
                    break;
                case 1:
                    builder.append(targetLocation.getBlockY()).append(" ");
                    break;
                case 2:
                    builder.append(targetLocation.getBlockZ());
                    break;
                default:
                    break;
            }
        }

        return builder.toString();
    }

    protected List<String> convertStringToMultiplesInput(String str) {
        String[] args = str.split(" ");
        List<String> result = new ArrayList<>();
        StringBuilder buffer = new StringBuilder(args[0]);
        result.add(buffer.toString());
        for (int i=1; i<args.length; i++) {
            buffer.append(" ").append(args[i]);
            result.add(buffer.toString());
        }

        return result;
    }

}
