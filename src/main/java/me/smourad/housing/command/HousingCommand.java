package me.smourad.housing.command;

import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class HousingCommand implements TabCompleter, CommandExecutor {

    @Getter
    private final String name;

    protected HousingCommand(String name) {
        this.name = name;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) {
            return onCommand((Player) sender, args);
        }

        sender.sendMessage("Cette commande ne peut être exécutée que par un joueur !");
        return false;
    }

    public abstract boolean onCommand(Player player, String[] args);

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) {
            return onTabComplete((Player) sender, args);
        }

        return null;
    }

    @Nullable
    public abstract List<String> onTabComplete(Player player, String[] args);

}
