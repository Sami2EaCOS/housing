package me.smourad.housing.command;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import me.smourad.housing.exception.HousingException;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;

import java.util.Set;

@Singleton
public final class TheCommandWatcher {

    @Inject
    public TheCommandWatcher(JavaPlugin plugin, Injector injector) {
        Reflections reflections = new Reflections("me.smourad.housing");
        Set<Class<? extends HousingCommand>> commands = reflections.getSubTypesOf(HousingCommand.class);

        try {
            for (Class<? extends HousingCommand> command : commands) {
                HousingCommand cmd = injector.getInstance(command);
                PluginCommand pluginCommand = plugin.getCommand(cmd.getName());

                pluginCommand.setExecutor(cmd);
                pluginCommand.setTabCompleter(cmd);
            }
        } catch (Exception e) {
            throw new HousingException(e);
        }
    }

}

