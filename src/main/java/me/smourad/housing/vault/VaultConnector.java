package me.smourad.housing.vault;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Getter;
import me.smourad.housing.exception.HousingException;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import javax.annotation.Nullable;

@Singleton
@Getter
public class VaultConnector {

    private final Economy economy;
    @Nullable
    private final Permission permissions;
    @Nullable
    private final Chat chat;

    @Inject
    public VaultConnector(JavaPlugin plugin) {
        economy = setupEconomy(plugin);
        permissions = setupPermissions(plugin);
        chat = setupChat(plugin);
    }

    private Economy setupEconomy(JavaPlugin plugin) {
        RegisteredServiceProvider<Economy> rsp = plugin
                .getServer()
                .getServicesManager()
                .getRegistration(Economy.class);

        if (rsp == null) {
            throw new HousingException();
        }

        return rsp.getProvider();
    }

    private Chat setupChat(JavaPlugin plugin) {
        RegisteredServiceProvider<Chat> rsp = plugin
                .getServer()
                .getServicesManager()
                .getRegistration(Chat.class);

        if (rsp == null) {
            return null;
        }

        return rsp.getProvider();
    }

    private Permission setupPermissions(JavaPlugin plugin) {
        RegisteredServiceProvider<Permission> rsp = plugin
                .getServer()
                .getServicesManager()
                .getRegistration(Permission.class);

        if (rsp == null) {
            return null;
        }

        return rsp.getProvider();
    }

}
